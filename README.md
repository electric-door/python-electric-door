# python electric door

For start this Python code run on a Raspberry Pi 3b (with the needed IO Ports 7 needed) you can used every you want.
We also need install / use python3

Clone this Repo on your home folder 
`cd ~`
`git clone https://gitlab.com/electric-door/python-electric-door.git`

All data you need modify are the "sample_config" rename this file to "config" and change the needed config.
Notice! on main_loop.py Line 15 you need define the Path to your "config" file, other stuff come then from the config file.

You can run the Server with "python3 main_loop.py" this will start the Server on Single run (if its crash you need start again per hand),
with "./start_server.sh" the Server will restart after a crash from alone, for this you need install "screen", to Stop the Server and kill a screens run "./stop_server.sh"

`sudo apt update`

`sudo apt install screen`

For Debug Reason you can start the server manuell with :

`python3 main_loop.py &>> crashlogs/crash_logs_per_day.log`

Server start with this debug as default , crash file getting overwritten by every Server start, so if Server crash by start check the file "crashlogs/crash_logs_per_day.log"

Also you need make some files executable "loop_live.sh", "start_server.sh", "stop_server.sh".

`sudo chmod +x loop_live.sh`

`sudo chmod +x start_server.sh`

`sudo chmod +x stop_server.sh`

I have set my RaspberryPi that he reboot every midnight to prefer lags and thats he gut stuck.

`sudo crontab -e`

`@midnight /sbin/shutdown -r now`

That the Server auto start after the reboot i have add the "start_server.sh" here: (if its possible *dont* use sudo as you may affect the permissions of the file)

`mkdir /home/pi/.config/autostart`

`nano /home/pi/.config/autostart/TerminalAutostart.desktop`

And add this to the new File: (Make sure that there is no empty line at beginn of the file)

"[Desktop Entry]

Type=Application

Name=Autostart-Electric-Door-Server

Comment=Start Server after Pi reboot 

Exec=/home/pi/python-electric-door/start_server.sh

Terminal=true"

After your Python Server and Hardware are running, you need install the Webpage and setup the Database:
https://gitlab.com/electric-door/php-electric-door


Edit: 
The 3d Files for the Hardware Case you can download here: 
https://www.tinkercad.com/things/3qPZbjIMBE8

The PCB and the needed Elektronic Parts as the circuit diagram i will upload later (not much time and i need dismantle my current Project for this)
Write me if you need help or you need the data earlier

Join me on my Discord Server: https://discord.gg/tdrMyt8

Best Regrads Bonny
