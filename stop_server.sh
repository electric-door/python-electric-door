#!/bin/sh

screen_name_server="check_door"
screen_name_live_loop="live_loop"
dt=$(date '+%d/%m/%Y %H:%M:%S')

echo "$dt ==>KILL: STOP Server and kill screen: $screen_name_live_loop + $screen_name_server"
screen -XS $screen_name_live_loop quit
screen -XS $screen_name_server quit