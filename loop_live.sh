#!/bin/sh

screen_name="check_door"

while true
do
	dt=$(date '+%d/%m/%Y %H:%M:%S')
	
	if [ $(screen -list | grep \\\.$screen_name | wc -l) != 1 ]
	then
		echo "$dt ==>INIT: Start Server with screenname: $screen_name"
		screen -dmS $screen_name /usr/bin/python3 /home/pi/python-electric-door/main_loop.py &>> /home/pi/python-electric-door/crashlogs/crash_logs_per_day.log
		sleep 10
	else
		timestamp=$(date +%s)
		file=`cat /home/pi/python-electric-door/server_live_status`
		
		diff=$((timestamp-file))
		
		if [ $diff -gt 30 ]
		then
			echo "$dt ==>ERROR: kill screen, server stuck, check logs!"
			screen -XS $screen_name quit
		else
			echo "$dt ==>PROCESS: ok all fine, server running!"
		fi
	fi
	sleep 30
done