#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import RPi.GPIO as GPIO
import time
import datetime
import urllib.request as urllib2
import configparser
import sys
from threading import Thread
GPIO.setwarnings(False)



none_config_root_folder = str("/home/pi/python-electric-door/")

#read the config file and set vars
parser = configparser.ConfigParser(inline_comment_prefixes="#")
parser.read(none_config_root_folder+"config")

turn_motor_left = int(parser.get("auto_door_config", "config_turn_motor_left"))
turn_motor_right = int(parser.get("auto_door_config", "config_turn_motor_right"))

piep = int(parser.get("auto_door_config", "config_piep"))

led_g = int(parser.get("auto_door_config", "config_led_g"))
led_r = int(parser.get("auto_door_config", "config_led_r"))
led_b = int(parser.get("auto_door_config", "config_led_b"))

magnet_input = int(parser.get("auto_door_config", "config_magnet_input"))

server_host =  str(parser.get("auto_door_config", "config_server_host"))
server_transfer_protocol =  str(parser.get("auto_door_config", "config_server_transfer_protocol"))

unlock_motor_runtime = float(parser.get("auto_door_config", "config_unlock_motor_runtime"))
lock_motor_runtime = float(parser.get("auto_door_config", "config_lock_motor_runtime"))

timer_piep_sound = float(parser.get("auto_door_config", "config_timer_piep_sound"))

root_folder =  str(parser.get("auto_door_config", "config_root_folder"))

server_url = str(server_transfer_protocol +"://"+server_host+"/")

lock_door_after_close = 0
wait_10_sec_befor_close = 0
live_loop = 0
start_programm = 0
loop = 0
error_loop = 0

# set pin directions
GPIO.setmode(GPIO.BCM)

GPIO.setup(turn_motor_left,GPIO.OUT)
GPIO.setup(turn_motor_right,GPIO.OUT)
GPIO.setup(piep,GPIO.OUT)  

GPIO.setup(led_r,GPIO.OUT)
GPIO.setup(led_g,GPIO.OUT)
GPIO.setup(led_b,GPIO.OUT)

GPIO.setup(magnet_input,GPIO.IN)

def write_logs(content):
    global start_programm
    now = datetime.datetime.now()
    log_date_time = now.strftime("%Y-%m-%d %H:%M:%S")
    log_date = now.strftime("%Y-%m-%d")
    
    file_log = open(root_folder+"logs/logs_file_" + log_date + ".txt","a")
    if start_programm == 1:
        file_log.write(str(log_date_time) +": "+ str(content + "\n"))
    else:
        start_programm = 1
        file_log.write("\n\n" + str(log_date_time) + ": Start of Programm....\n" + str(log_date_time) +": "+ str(content + "\n"))
        
    file_log.close()
    
    print(str(log_date_time) +": "+ str(content + "\n"))

write_logs("int GPIO")

def led_out():
    GPIO.output(led_b,0)
    GPIO.output(led_g,0)
    GPIO.output(led_r,0)
    return;

def led_display_on(color):
    #reset old led
    led_out()
    
    if color == "blue":
        GPIO.output(led_b,1)
    if color == "green":
        GPIO.output(led_g,1)
    if color == "red":
        GPIO.output(led_r,1)
    if color == "yellow":
        GPIO.output(led_g,1)
        GPIO.output(led_r,1)
    return;

#load_old_status_from_file
file = open(root_folder+"door_status", "r")
door_status = str(file.read())

write_logs("load local door status from file: door_status ("+str(door_status)+")")

#load_old_chron_jobs_from_file
file_chron = open(root_folder+"chron_job_unlocked", "r")
lock_door_after_close = int(file_chron.read())

write_logs("load local chron job status from file: chron_job_unlocked ("+str(lock_door_after_close)+")")

def write_chron_job_task(status):
    #save chron_job_task at file when server reboot / crash
    file = open(root_folder+"chron_job_unlocked","w")
    file.write(status)
    file.close()
    
    lock_door_after_close = status
    return;

#set led to right color by start server
if door_status == "unlocked":
    led_display_on("green")
else:
    led_display_on("red")

file_oc = open(root_folder+"door_status_op", "r")
door_open_close = str(file_oc.read())

write_logs("load local lock / unlock status from file: door_status_op ("+str(door_open_close)+")")

def write_file(status):
    global door_status
    
    file = open(root_folder+"door_status","w")
    file.write(status)
    file.close()
    
    door_status = status
    return;

def send_feedback(id,feedback):
    write_logs(id +" "+ feedback)
    urllib2.urlopen(server_url+"write.php?door_status="+feedback+"&door_id="+id)
    return;

def lock(id,forced):
    global door_status
    write_logs("ask_for_lock_forced? " + forced)
    if GPIO.input(magnet_input) == GPIO.LOW or forced == "true":
        if door_status != "locked" or forced == "true":
            turn_motor_left_action(lock_motor_runtime)
            if id != "none_id":
                send_feedback(id,"ok_door_locked")
            
            led_display_on("red")
            write_file("locked")
            urllib2.urlopen(server_url+"write_status.php?door_status=locked")
            piep_sec(timer_piep_sound)
        else:
            send_feedback(id,"error_already_locked")
    else:
       send_feedback(id,"error_door_open")
       
    time.sleep(1)
    return;

def unlock(id, forced):
    global door_status
    write_logs("ask_for_unlock_forced? " + forced)
    time.sleep(1)
    if GPIO.input(magnet_input) == GPIO.LOW or forced == "true":
        if door_status != "unlocked" or forced == "true":
            turn_motor_right_action(unlock_motor_runtime)
            if id != "none_id":
                send_feedback(id,"ok_door_unlocked")
            
            led_display_on("green")
            write_file("unlocked")
            urllib2.urlopen(server_url+"write_status.php?door_status=unlocked")
            piep_sec(timer_piep_sound)
        else:
            send_feedback(id,"error_already_unlocked")
    else:
       send_feedback(id,"error_door_open")
       
    time.sleep(1)
    return;

def piep_sec(secound):
    if(secound != 0):
        GPIO.output(piep,1)
        time.sleep(secound)
        GPIO.output(piep,0)
    return;
    
def turn_motor_right_action(secound):
    GPIO.output(turn_motor_right,1)
    time.sleep(secound)
    GPIO.output(turn_motor_right,0)
    return;
    
def turn_motor_left_action(secound):
    GPIO.output(turn_motor_left,1)
    time.sleep(secound)
    GPIO.output(turn_motor_left,0)
    return;

def check_door_close_open():
    global door_open_close
    global lock_door_after_close
    global wait_10_sec_befor_close
      
    if GPIO.input(magnet_input) == GPIO.LOW:
        door_open_close_now = "closed"
    else:
        door_open_close_now = "opened"
        led_display_on("blue")
    
    if door_open_close != door_open_close_now:
            urllib2.urlopen(server_url+"write_status.php?door_status="+door_open_close_now)
            
            if door_open_close == "opened" and lock_door_after_close == 1:
                wait_10_sec_befor_close = 1
                write_logs("door_open_wait_for_close_and_lock")
            
            if door_open_close_now == "closed":
                if door_status == "unlocked":
                    led_display_on("green")
                else:
                    led_display_on("red")
            
            door_open_close = door_open_close_now
            time_now = int(time.time())
            write_logs("door_change "+door_open_close)
            file = open(root_folder+"door_status_op","w")
            file.write(door_open_close)
            file.close()
                
    return;

def open_website(url):
    return urllib2.urlopen(url);

write_logs("######### read_config......##########")
write_logs("config_turn_motor_left:"+str(turn_motor_left))
write_logs("config_turn_motor_right:"+str(turn_motor_right))
write_logs("config_unlock_motor_runtime:"+str(unlock_motor_runtime))
write_logs("config_lock_motor_runtime:"+str(lock_motor_runtime))
write_logs("config_piep:"+str(piep))
write_logs("config_timer_piep_sound:"+str(timer_piep_sound))
write_logs("config_led_g:"+str(led_g))
write_logs("config_led_r:"+str(led_r))
write_logs("config_led_b:"+str(led_b))
write_logs("config_magnet_input:"+str(magnet_input))
write_logs("config_server_host:"+server_host)
write_logs("config_server_transfer_protocol:"+server_transfer_protocol)
write_logs("server_url:"+server_url)
write_logs("config_root_folder:"+root_folder)
write_logs("######### end......##########")

write_logs("finish all functions")
write_logs("Start Main loop.... ")

while True:
    try:
        time.sleep(1)
        time_now = int(time.time())
        #print(time_now)
        
        check_door_close_open()
        
        if wait_10_sec_befor_close == 1:
            write_logs("wait_for_lock_door_after_close" + str(loop))
            if loop >= 10:
                if GPIO.input(magnet_input) == GPIO.LOW:
                    wait_10_sec_befor_close = 0
                    lock("none_id", "false")
                    write_logs("door_closed_for_10_sec_now_lock")
                    write_chron_job_task(0)
                    loop = 0
                else:
                    if error_loop >= 2 :
                        write_logs("final_abort_because_door_still_open")
                        wait_10_sec_befor_close = 1
                        write_chron_job_task(0)
                        loop = 0
                        error_loop = 0
                    else:
                        write_logs("error_door_open_try_again_in_10_sec")
                        wait_10_sec_befor_close = 1
                        error_loop = error_loop +1
            else:
                loop = loop +1
        
        web_check_commands = server_url+"read_data.php"
        #row_data_page = urllib2.urlopen(web_check_commands)
        #print(t.read())
        
        request_url = urllib2.Request(web_check_commands)
        try:
            row_data_page = urllib2.urlopen(request_url, timeout=10)
        except urllib2.URLError as e:
            write_logs ("Bad URL or timeout on url: " + str(web_check_commands) + " Error : "+str(e.reason))
            continue # skips to the next iteration of the loop
        except urllib2.HTTPError as e:
            write_logs ("HTTPError : " + str(e.code))
            continue # skips to the next iteration of the loop
        except Exception:
            write_logs ("Undefine Error on urllib2")
            continue # skips to the next iteration of the loop
        
        if live_loop >= 5:
            Thread(target=open_website, args=[server_url+"write_online.php"]).start()
            live_loop = 0
            
            #write ate local file last time server are online
            now = round(time.time())
            timestamp = str(now)
            #print("timestamp =", timestamp)
            
            file = open(root_folder+"server_live_status","w")
            file.write(timestamp)
            file.close()
        else:
            live_loop = live_loop + 1
        
        
        resp_page = row_data_page.read()
        
        result_commands = str(resp_page)
        result_commands = result_commands.replace("b'", "")
        result_commands = result_commands.replace("'", "")
        
        #print(result_commands)
        if result_commands != "empty" and result_commands != "":
            split_array = result_commands.split('|')
            array_count = len(split_array)
            
            #print(split_array[0])
            
            count_i = -1
            for count_i in range(array_count):
                #print(count_i)
                commands_split = split_array[count_i].split(';')
                
                #print(commands_split[0])
                
                if commands_split[1] == "now":
                    if commands_split[2] == "lock":
                        lock(commands_split[0],"false")
                        #print("lock_now")
                
                    if commands_split[2] == "unlock":
                        unlock(commands_split[0],"false")
                        #print("unlock_now")
                        
                    if commands_split[2] == "lock_forced":
                        lock(commands_split[0],"true")
                        #print("lock_now_forced")
                
                    if commands_split[2] == "unlock_forced":
                        unlock(commands_split[0],"true")
                        #print("unlock_now_forced")
                
                if commands_split[1] == "job":
                    time_stamp_command = int(commands_split[3])
                    #print("wait_for_chron_job " + str(time_stamp_command) + " current: " + str(time_now))
                    if commands_split[2] == "unlock" and time_now >= time_stamp_command:
                        unlock(commands_split[0], "false")
                        write_logs("chron_job_unlocked")
                        if commands_split[4] == "lock_after_close":
                            write_chron_job_task(1)
             
        
    except Exception as e:
        #logging.error('Error occurred ' + str(e))
        crash=["Error on line {}".format(sys.exc_info()[-1].tb_lineno),"\n",e]
        #print(crash)
        crash_now = datetime.datetime.now()
        crash_date_time = crash_now.strftime("%Y-%m-%d %H:%M:%S")
        with open(root_folder+"crashlogs/CRASH-"+crash_date_time+".txt","w") as crashLog:
            for i in crash:
                i=str(i)
                crashLog.write(i)
        write_logs("..CRASH HARD END of Programm")
        GPIO.cleanup()
        raise
    
    except KeyboardInterrupt:
        write_logs("..HARD END of Programm")
        GPIO.cleanup()
        raise