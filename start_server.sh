#!/bin/sh

UPSTREAM=${1:-'@{u}'}
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse "$UPSTREAM")
BASE=$(git merge-base @ "$UPSTREAM")

if [ $LOCAL = $REMOTE ]; then
	echo "Server Version are Up-to-date"
elif [ $LOCAL = $BASE ]; then
	echo "New Server Version are avaible pls run 'cd ~python-electric-door' 'git pull'"
fi

dt=$(date '+%d/%m/%Y %H:%M:%S')
config_file=/home/pi/python-electric-door/config

if [ -f "$config_file" ]; then
	echo "$dt ==>INIT: Found config file '$config_file', Try start Server "
else
	echo "$dt ==>ERROR: no '$config_file' file found! Pls copy the 'sample_config' rename it to 'config' and modify the parms as you need."
	echo "$dt ==>ERROR: Server can´t start."
	exit
fi

screen_name_live_loop="live_loop"
need_start_server=0

if [ $(screen -list | grep \\\.$screen_name_live_loop | wc -l) != 1 ]
then
	need_start_server=1
else
	echo "$dt ==>ERROR: $screen_name_live_loo already alive, kill it!"
	screen -XS $screen_name_live_loop quit
	need_start_server=1
	sleep 1
fi
	
if [ $need_start_server -eq 1 ];
then
	echo "$dt ==>INIT: Start Server with screenname: $screen_name_live_loop"
	screen -dmS $screen_name_live_loop /home/pi/python-electric-door/loop_live.sh
fi
